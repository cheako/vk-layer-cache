// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod types;

pub(crate) mod graphics_pipeline;
pub(crate) mod render_pass;

use super::DEVICE;

#[allow(unreachable_code)]
#[allow(unused_variables)]
pub(crate) fn end_frame(device: &ash::Device) {
    graphics_pipeline::GRAPHICS_PIPELINE
        .lock()
        .unwrap()
        .end_frame();
    render_pass::RENDER_PASS.lock().unwrap().end_frame();
}
