// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::panic::catch_unwind;
use std::sync::LazyLock;
use std::sync::Mutex;

use ash::vk;

use super::types::*;

pub(crate) struct ImageViews(
    pub(crate) HashMap<vk::ImageView, MyImageViewCreateInfo>,
    pub(crate) HashMap<MyImageViewCreateInfo, (Vec<vk::ImageView>, ash::Device)>,
    pub(crate) bool,
);

#[allow(clippy::type_complexity)]
pub(crate) static IMAGE_VIEW: LazyLock<Mutex<ImageViews>> = LazyLock::new(|| {
    ImageViews(
        Default::default(),
        Default::default(),
        std::env::var("CHEAKO_I").is_ok() && dbg!(std::env::var("CHEAKO_V").is_ok()),
    )
    .into()
});

impl ImageViews {
    pub(crate) fn end_frame(&mut self) {
        for (_, (objects, device)) in self.1.drain() {
            for image_view in objects.into_iter() {
                dbg!("flush".len());
                unsafe { device.destroy_image_view(image_view, None) }
            }
        }
    }

    pub(crate) fn flush_image(&mut self, image: vk::Image) {
        for (_, (objects, device)) in self.1.drain().filter(|(x, _)| x.image == image) {
            for image_view in objects.into_iter() {
                dbg!("upstream leaving".len());
                unsafe { device.destroy_image_view(image_view, None) }
            }
        }
    }
}

pub(crate) unsafe extern "system" fn create_image_view(
    device: vk::Device,
    p_create_info: *const vk::ImageViewCreateInfo,
    p_allocator: *const vk::AllocationCallbacks,
    p_image_view: *mut vk::ImageView,
) -> vk::Result {
    let result = catch_unwind(|| {
        let create_info: MyImageViewCreateInfo = p_create_info.into();

        let go = || {
            dbg!("creating".len());
            super::DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .device
                .create_image_view(p_create_info.as_ref().unwrap(), p_allocator.as_ref())
        };

        if !create_info.more {
            use std::collections::hash_map::Entry::*;
            let mut h = IMAGE_VIEW.lock().unwrap();
            let enabled = h.2;
            match h.1.entry(create_info) {
                Occupied(mut x) if enabled => {
                    dbg!("from cache".len());
                    let xa = x.get_mut().0.pop();
                    xa.map_or_else(go, Result::Ok)
                }
                _ => go(),
            }
            .map(|x| {
                h.0.insert(x, create_info);
                *p_image_view = x;
                vk::Result::SUCCESS
            })
        } else {
            dbg!("skipping cache".len());
            go().map(|x| {
                *p_image_view = x;
                vk::Result::SUCCESS
            })
        }
    });
    match result.unwrap() {
        Err(x) => x,
        Ok(x) => x,
    }
}

pub(crate) unsafe extern "system" fn destroy_image_view(
    device: vk::Device,
    image_view: vk::ImageView,
    p_allocator: *const vk::AllocationCallbacks,
) {
    let result = catch_unwind(|| {
        let mut h = IMAGE_VIEW.lock().unwrap();
        if h.2
            && let Some(y) = h.0.remove(&image_view)
        {
            dbg!("save".len());
            use std::collections::hash_map::Entry::*;
            match h.1.entry(y) {
                Occupied(mut x) => {
                    x.get_mut().0.push(image_view);
                }
                Vacant(x) => {
                    x.insert((
                        vec![image_view],
                        super::DEVICE
                            .read()
                            .unwrap()
                            .get(&device)
                            .unwrap()
                            .device
                            .clone(),
                    ));
                }
            }
        } else {
            dbg!("destroy".len());
            super::DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .device
                .destroy_image_view(image_view, p_allocator.as_ref())
        }
    });
    result.unwrap();
}
