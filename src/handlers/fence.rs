// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::panic::catch_unwind;
use std::sync::LazyLock;
use std::sync::Mutex;

use ash::vk;

use super::types::*;

pub(super) struct Fences(
    pub(super) HashMap<vk::Fence, MyFenceCreateInfo>,
    pub(super) HashMap<MyFenceCreateInfo, Vec<vk::Fence>>,
    pub(super) bool,
);

#[allow(clippy::type_complexity)]
pub(super) static FENCE: LazyLock<Mutex<Fences>> = LazyLock::new(|| {
    Fences(
        Default::default(),
        Default::default(),
        dbg!(std::env::var("CHEAKO_F").is_ok()),
    )
    .into()
});

impl Fences {
    pub(crate) fn end_frame(&mut self, device: &ash::Device) {
        for (_, objects) in self.1.drain() {
            for fence in objects.into_iter() {
                dbg!("flush".len());
                unsafe { device.destroy_fence(fence, None) }
            }
        }
    }
}

pub(crate) unsafe extern "system" fn create_fence(
    device: vk::Device,
    p_create_info: *const vk::FenceCreateInfo,
    p_allocator: *const vk::AllocationCallbacks,
    p_fence: *mut vk::Fence,
) -> vk::Result {
    let result = catch_unwind(|| {
        let create_info: MyFenceCreateInfo = p_create_info.into();

        let go = || {
            dbg!("creating".len());
            super::DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .device
                .create_fence(p_create_info.as_ref().unwrap(), p_allocator.as_ref())
        };

        if p_create_info.read().p_next.is_null() {
            use std::collections::hash_map::Entry::*;
            let mut h = FENCE.lock().unwrap();
            let enabled = h.2;
            match h.1.entry(create_info) {
                Occupied(mut x) if enabled => {
                    dbg!("from cache".len());
                    let xa = x.get_mut().pop();
                    xa.map_or_else(go, Result::Ok)
                }
                _ => go(),
            }
            .map(|x| {
                h.0.insert(x, create_info);
                *p_fence = x;
                vk::Result::SUCCESS
            })
        } else {
            dbg!("skipping cache".len());
            go().map(|x| {
                *p_fence = x;
                vk::Result::SUCCESS
            })
        }
    });
    match result.unwrap() {
        Err(x) => x,
        Ok(x) => x,
    }
}

pub(crate) unsafe extern "system" fn destroy_fence(
    device: vk::Device,
    fence: vk::Fence,
    p_allocator: *const vk::AllocationCallbacks,
) {
    let result = catch_unwind(|| {
        let mut h = FENCE.lock().unwrap();
        if h.2
            && let Some(y) = h.0.remove(&fence)
        {
            dbg!("save".len());
            use std::collections::hash_map::Entry::*;
            super::DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .device
                .reset_fences(std::slice::from_ref(&fence))
                .unwrap();
            match h.1.entry(y) {
                Occupied(mut x) => {
                    x.get_mut().push(fence);
                }
                Vacant(x) => {
                    x.insert(vec![fence]);
                }
            }
        } else {
            dbg!("destroy".len());
            super::DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .device
                .destroy_fence(fence, p_allocator.as_ref())
        }
    });
    assert!(result.is_ok());
    result.unwrap()
}
