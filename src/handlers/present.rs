// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![allow(unused_imports)]

use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, Read},
    panic::catch_unwind,
    sync::{
        mpsc::{self, Receiver},
        LazyLock, Mutex, RwLock,
    },
    time::{Duration, Instant},
};

use ash::vk;
use timeout_readwrite::TimeoutReader;

struct Queue {
    khr_swapchain_fn: ash::khr::swapchain::DeviceFn,
    device: ash::Device,
}

#[allow(clippy::type_complexity)]
static QUEUE: LazyLock<RwLock<HashMap<vk::Queue, Queue>>> = LazyLock::new(Default::default);

#[allow(clippy::type_complexity)]
pub(crate) static PERIODIC: LazyLock<RwLock<Option<Duration>>> = LazyLock::new(|| {
    fn parse(x: String) -> Duration {
        Duration::from_secs(x.parse::<u64>().unwrap_or(2))
    }

    RwLock::new(dbg!(std::env::var("CHEAKO_SECONDS").map(parse)).ok())
});

#[allow(clippy::type_complexity)]
pub(crate) static TRIGGER: LazyLock<(Mutex<mpsc::Sender<()>>, Mutex<mpsc::Receiver<()>>)> =
    LazyLock::new(|| {
        let (s, r) = mpsc::channel();
        (Mutex::new(s), Mutex::new(r))
    });

pub(crate) unsafe extern "system" fn get_device_queue(
    device: vk::Device,
    queue_family_index: u32,
    queue_index: u32,
    p_queue: *mut vk::Queue,
) {
    let global = super::DEVICE.read().unwrap();
    let object = global.get(&device).unwrap();
    let khr_swapchain_fn = object.khr_swapchain_fn.clone();
    let device = object.device.clone();

    let queue = unsafe { device.get_device_queue(queue_family_index, queue_index) };

    QUEUE.write().unwrap().insert(
        queue,
        Queue {
            khr_swapchain_fn,
            device,
        },
    );
    unsafe { *p_queue = queue };
}

#[allow(dead_code)]
fn line_count(path: impl AsRef<std::path::Path>) -> usize {
    if let Ok(inner) = std::fs::File::open(path) {
        let file = std::io::BufReader::new(inner);
        file.lines()
            .filter_map(Result::ok)
            .filter(|x| x.ends_with("used"))
            .count()
    } else {
        0
    }
}

fn get_clk() -> String {
    if let Ok(inner) = std::fs::File::open("/sys/class/drm/card0/device/pp_dpm_sclk") {
        let file = std::io::BufReader::new(inner);
        file.lines()
            .filter_map(Result::ok)
            .filter(|x| x.ends_with('*'))
            .collect()
    } else {
        "".to_owned()
    }
}

fn log_watcher() -> String {
    #[allow(unused_mut)]
    let mut buf = String::new();
    #[cfg(not)]
    {
        static DATA: LazyLock<std::io::Result<Mutex<TimeoutReader<File>>>> = LazyLock::new(|| {
            File::open("debug/tracing/trace_pipe")
                .map(|file| Mutex::new(TimeoutReader::new(file, Duration::SECOND / 300)))
        });
        if let Ok(Ok(mut rdr)) = DATA.as_ref().map(Mutex::lock) {
            let _ = rdr.read_to_string(&mut buf);
        }
    }
    buf
}

pub(crate) static WAIT_FOR_FENCES: LazyLock<RwLock<Vec<Duration>>> =
    LazyLock::new(Default::default);
pub(crate) unsafe extern "system" fn wait_for_fences(
    device: vk::Device,
    len: u32,
    data_address: *const vk::Fence,
    wait_all: u32,
    timeout: u64,
) -> vk::Result {
    let result = catch_unwind(|| {
        let global = super::DEVICE.read().unwrap();
        let object = global.get(&device).unwrap();

        let start = Instant::now();
        let ret = object.device.wait_for_fences(
            core::slice::from_raw_parts(data_address, len as _),
            wait_all != 0,
            timeout,
        );
        WAIT_FOR_FENCES.write().unwrap().push(start.elapsed());

        ret
    });
    match result.unwrap() {
        Err(x) => x,
        Ok(x) => vk::Result::SUCCESS,
    }
}

pub(crate) static ACQUIRE_NEXT_IMAGE: LazyLock<RwLock<Vec<Duration>>> =
    LazyLock::new(Default::default);
pub(crate) unsafe extern "system" fn acquire_next_image(
    device: vk::Device,
    swapchain: vk::SwapchainKHR,
    timeout: u64,
    semaphore: vk::Semaphore,
    fence: vk::Fence,
    p_image_index: *mut u32,
) -> vk::Result {
    let result = catch_unwind(|| {
        let global = super::DEVICE.read().unwrap();
        let object = global.get(&device).unwrap();
        let khr_swapchain_fn = object.khr_swapchain_fn.clone();
        let start = Instant::now();
        let ret = unsafe {
            (khr_swapchain_fn.acquire_next_image_khr)(
                device,
                swapchain,
                timeout,
                semaphore,
                fence,
                p_image_index,
            )
        };
        ACQUIRE_NEXT_IMAGE.write().unwrap().push(start.elapsed());

        ret
    });
    result.unwrap()
}

pub(crate) unsafe extern "system" fn queue_present(
    queue: vk::Queue,
    p_present_info: *const vk::PresentInfoKHR,
) -> vk::Result {
    let global = QUEUE.read().unwrap();
    let object = global.get(&queue).unwrap();
    let ret = unsafe { (object.khr_swapchain_fn.queue_present_khr)(queue, p_present_info) };

    {
        super::semaphore::SEMAPHORE
            .lock()
            .unwrap()
            .end_frame(&object.device);
        super::fence::FENCE
            .lock()
            .unwrap()
            .end_frame(&object.device);
        super::buffer::BUFFER
            .lock()
            .unwrap()
            .end_frame(&object.device);
        super::memory::MEMORY
            .lock()
            .unwrap()
            .end_frame(&object.device);
        super::images::end_frame(&object.device);
        super::renders::end_frame(&object.device);
        super::framebuffer::end_frame(&object.device);
    }

    let now = Instant::now();
    static mut LAST: Option<Instant> = None;

    /// Log timestamp reference
    static mut FIRST: Option<Instant> = None;
    unsafe {
        if FIRST.is_none() {
            FIRST = Some(now);
        }
    }

    /// (When to next capture state, Captured state)
    #[allow(clippy::type_complexity)]
    static DEBUG: LazyLock<(Mutex<Option<Instant>>, RwLock<String>)> =
        LazyLock::new(Default::default);

    let mut timer = DEBUG.0.lock().unwrap();

    if timer.is_some_and(|x| x < now) {
        let mut t = DEBUG.1.write().unwrap();
        *t = format!(
            "{}, {}, {}\n",
            humantime::format_duration(now - unsafe { FIRST }.unwrap()),
            get_clk(),
            log_watcher()
        );
        // Disable the above check until replaced below
        timer.take();
    }

    /// Once for each frame, until time out
    static mut COUNTER: u32 = 0;
    unsafe { COUNTER += 1 };
    static mut TOTAL: u32 = 0;
    unsafe { TOTAL = TOTAL.wrapping_add(1) };

    static mut PAD_TIME: Duration = Duration::ZERO;

    let run_time = humantime::format_duration(now - unsafe { FIRST }.unwrap()).to_string();

    /// Keeps track of when COUNTER should next be reset
    /// Only None at the beginning
    static mut FPS_TIMER: Option<Instant> = None;
    match unsafe { FPS_TIMER } {
        Some(future_timeout) if future_timeout + unsafe { PAD_TIME } > now => {
            // Not the first frame.
            if let Some(last) = unsafe { LAST } {
                let time = unsafe { PAD_TIME } + Duration::from_secs(2)
                    - (future_timeout + unsafe { PAD_TIME }).saturating_duration_since(now);
                let duration = now.saturating_duration_since(last);
                // We've already incremented COUNTER.
                let avg = time / unsafe { COUNTER };
                // More than 20%?
                let should_ignore = (avg + unsafe { PAD_TIME }) * 12 < duration * 10;
                if should_ignore {
                    let (time, duration, avg, pad_time) = (
                        humantime::format_duration(time).to_string(),
                        humantime::format_duration(duration).to_string(),
                        humantime::format_duration(avg).to_string(),
                        humantime::format_duration(std::mem::replace(
                            unsafe { &mut PAD_TIME },
                            duration - avg,
                        ))
                        .to_string(),
                    );
                    dbg!(
                        "Ignoring an unusually long frame:".len(),
                        unsafe { COUNTER },
                        time,
                        duration,
                        avg,
                        pad_time,
                    );
                }
            }
            unsafe { LAST = Some(now) };
            ret
        }
        Some(reached_or_past_timeout) => {
            // How many seconds have passed?
            let time = now
                .saturating_duration_since(reached_or_past_timeout + unsafe { PAD_TIME })
                .as_secs_f64()
                + 2.;
            let fps = unsafe { COUNTER } as f64 / time;

            static TARGET_FPS: LazyLock<Option<f64>> = LazyLock::new(|| {
                dbg!(std::env::var("CHEAKO_TARGET_FPS").map(|x| x.parse::<f64>().unwrap_or(22.)))
                    .ok()
            });

            let should_optimize = TARGET_FPS.filter(|x| fps < *x).is_some();

            let (pad_time, time) = (
                humantime::format_duration(unsafe { PAD_TIME }).to_string(),
                humantime::format_duration(Duration::from_secs_f64(time)).to_string(),
            );

            let (mut wait_for_fences, mut acquire_next_image) = (
                WAIT_FOR_FENCES.write().unwrap(),
                ACQUIRE_NEXT_IMAGE.write().unwrap(),
            );
            dbg!(
                unsafe { TOTAL },
                unsafe { COUNTER },
                &run_time,
                pad_time,
                time,
                should_optimize,
                fps,
                fps.recip(),
                wait_for_fences.len(),
                wait_for_fences.iter().min(),
                wait_for_fences.iter().max(),
                acquire_next_image.len(),
                acquire_next_image.iter().min(),
                acquire_next_image.iter().max(),
            );
            wait_for_fences.clear();
            acquire_next_image.clear();

            unsafe {
                FPS_TIMER = Some(now + Duration::from_secs(2));
                PAD_TIME = Duration::ZERO;
                COUNTER = 0;
            }

            drop(dbg!(DEBUG.1.read().unwrap()));
            assert_eq!(
                timer.replace(now + Duration::SECOND / 30).map(|x| format!(
                    "{}",
                    humantime::format_duration(x - unsafe { FIRST }.unwrap())
                )),
                None
            );

            // Discard data until now.
            log_watcher();
            let _ = std::env::var("MESA_VK_MEMORY_TRACE_TRIGGER").map(File::create);

            if should_optimize {
                let replacing_with_suboptimal = ret;
                let _ = dbg!(replacing_with_suboptimal);
                return vk::Result::SUBOPTIMAL_KHR;
            }

            /// Calculate user injected suboptimal
            static mut USER_TIMESTAMP: Option<Instant> = None;

            // User instant suboptimal
            if let Ok(Ok(())) = TRIGGER.1.lock().map(|x| Receiver::try_recv(&x)) {
                unsafe { USER_TIMESTAMP = Some(now) };
                let replacing_with_suboptimal = ret;
                let _ = dbg!(run_time, replacing_with_suboptimal);
                return vk::Result::SUBOPTIMAL_KHR;
            }

            unsafe { LAST = Some(now) };

            // User has configured suboptimal
            if let Some(limit) = *PERIODIC.read().unwrap() {
                match unsafe { USER_TIMESTAMP } {
                    // Is past limit
                    Some(ctr) if limit < now.duration_since(ctr) => {
                        unsafe { USER_TIMESTAMP = Some(now) };
                        let replacing_with_suboptimal = ret;
                        let _ = dbg!(run_time, replacing_with_suboptimal);
                        #[allow(clippy::needless_return)]
                        return vk::Result::SUBOPTIMAL_KHR;
                    }
                    None => {
                        // Set on first time
                        unsafe { USER_TIMESTAMP = Some(now) };
                        ret
                    }
                    _ => ret,
                }
            } else {
                ret
            }
        }
        None => {
            unsafe { LAST = Some(now) };
            unsafe { FPS_TIMER = Some(now + Duration::from_secs(2)) };
            ret
        }
    }
}
