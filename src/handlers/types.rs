// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use ash::vk;
use std::{ffi::c_void, slice::from_raw_parts};

pub struct VkIter<'a>(Option<*const vk::BaseInStructure<'a>>);

impl<'a> VkIter<'a> {
    pub fn new(inner: *const c_void) -> Self {
        Self(unsafe { inner.cast::<vk::BaseInStructure>().as_ref() }.map(|x| x as *const _))
    }
}

impl<'a> Iterator for VkIter<'a> {
    type Item = *const vk::BaseInStructure<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.0 = self.0.and_then(|x| match unsafe { x.read() }.p_next {
            x if !x.is_null() => Some(x),
            _ => None,
        });
        self.0
    }
}

pub struct FromVk<F>(pub u32, pub *const F);

impl<F, T> From<FromVk<F>> for Box<[T]>
where
    F: Copy + Into<T>,
{
    fn from(FromVk(count, ptr): FromVk<F>) -> Self {
        if ptr.addr() == 0 || count == 0 {
            Box::new([])
        } else {
            unsafe { from_raw_parts(ptr, count as _) }
                .iter()
                .copied()
                .map(Into::into)
                .collect()
        }
    }
}

pub struct FromVk2<F1, F2>(pub u32, pub *const F1, pub *const F2);

impl<F1, F2, T1, T2> From<FromVk2<F1, F2>> for Box<[(T1, T2)]>
where
    F1: Copy + Into<T1>,
    F2: Copy + Into<T2>,
{
    fn from(FromVk2(count, ptr1, ptr2): FromVk2<F1, F2>) -> Self {
        unsafe { from_raw_parts(ptr1, count as _) }
            .iter()
            .copied()
            .map(Into::into)
            .zip(
                unsafe { from_raw_parts(ptr2, count as _) }
                    .iter()
                    .copied()
                    .map(Into::into),
            )
            .collect()
    }
}

#[derive(Copy, Clone, Hash, PartialEq, Eq)]
pub struct MyMemoryAllocateInfo {
    pub allocation_size: vk::DeviceSize,
    pub memory_type_index: u32,
    pub priority: Option<u32>,
    pub image: Option<vk::Image>,
    pub buffer: Option<vk::Buffer>,
}

impl<'a> From<*const vk::MemoryAllocateInfo<'a>> for MyMemoryAllocateInfo {
    fn from(this: *const vk::MemoryAllocateInfo<'a>) -> Self {
        let this = unsafe { this.read() };
        let (allocation_size, memory_type_index) = (this.allocation_size, this.memory_type_index);

        let (mut priority, mut image, mut buffer) = Default::default();

        for i in VkIter::new(this.p_next) {
            match unsafe { i.read() }.s_type {
                vk::StructureType::MEMORY_PRIORITY_ALLOCATE_INFO_EXT => {
                    priority = Some(
                        unsafe { i.cast::<vk::MemoryPriorityAllocateInfoEXT>().read() }
                            .priority
                            .to_bits(),
                    );
                }
                vk::StructureType::MEMORY_DEDICATED_ALLOCATE_INFO => {
                    let i = unsafe { i.cast::<vk::MemoryDedicatedAllocateInfo>().read() };
                    if i.image != vk::Image::null() {
                        image = Some(i.image);
                    }
                    if i.buffer != vk::Buffer::null() {
                        buffer = Some(i.buffer);
                    }
                }
                _ => {}
            }
        }

        Self {
            allocation_size,
            memory_type_index,
            priority,
            image,
            buffer,
        }
    }
}

#[derive(Copy, Clone, Hash, PartialEq, Eq)]
pub struct MySemaphoreCreateInfo(pub vk::SemaphoreCreateFlags);

impl<'a> From<*const vk::SemaphoreCreateInfo<'a>> for MySemaphoreCreateInfo {
    fn from(this: *const vk::SemaphoreCreateInfo) -> Self {
        Self(unsafe { this.as_ref() }.unwrap().flags)
    }
}

#[derive(Copy, Clone, Hash, PartialEq, Eq)]
pub struct MyFenceCreateInfo(pub vk::FenceCreateFlags);

impl<'a> From<*const vk::FenceCreateInfo<'a>> for MyFenceCreateInfo {
    fn from(this: *const vk::FenceCreateInfo) -> Self {
        Self(unsafe { this.as_ref() }.unwrap().flags)
    }
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyFramebufferCreateInfo {
    pub more: bool,
    pub flags: vk::FramebufferCreateFlags,
    pub render_pass: vk::RenderPass,
    pub attachments: Box<[vk::ImageView]>,
    pub width: u32,
    pub height: u32,
    pub layers: u32,
}

impl<'a> From<*const vk::FramebufferCreateInfo<'a>> for MyFramebufferCreateInfo {
    fn from(this: *const vk::FramebufferCreateInfo) -> Self {
        let this = unsafe { this.read() };
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            render_pass: this.render_pass,
            attachments: FromVk(this.attachment_count, this.p_attachments).into(),
            width: this.width,
            height: this.height,
            layers: this.layers,
        }
    }
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyBufferCreateInfo {
    pub more: bool,
    pub flags: vk::BufferCreateFlags,
    pub size: vk::DeviceSize,
    pub usage: vk::BufferUsageFlags,
    pub sharing_mode: vk::SharingMode,
    pub queue_family_indices: Box<[u32]>,
}

impl<'a> From<*const vk::BufferCreateInfo<'a>> for MyBufferCreateInfo {
    fn from(this: *const vk::BufferCreateInfo) -> Self {
        let this = unsafe { this.read() };
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            size: this.size,
            usage: this.usage,
            sharing_mode: this.sharing_mode,
            queue_family_indices: FromVk(
                this.queue_family_index_count,
                this.p_queue_family_indices,
            )
            .into(),
        }
    }
}
