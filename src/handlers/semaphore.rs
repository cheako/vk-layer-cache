// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::panic::catch_unwind;
use std::sync::LazyLock;
use std::sync::Mutex;

use ash::vk;

use super::types::*;

pub(super) struct Semaphores(
    pub(super) HashMap<vk::Semaphore, MySemaphoreCreateInfo>,
    pub(super) HashMap<MySemaphoreCreateInfo, Vec<vk::Semaphore>>,
    pub(super) bool,
);

pub(super) static SEMAPHORE: LazyLock<Mutex<Semaphores>> = LazyLock::new(|| {
    Semaphores(
        Default::default(),
        Default::default(),
        dbg!(std::env::var("CHEAKO_S").is_ok()),
    )
    .into()
});

impl Semaphores {
    pub(crate) fn end_frame(&mut self, device: &ash::Device) {
        for (_, objects) in self.1.drain() {
            for semaphore in objects.into_iter() {
                dbg!("flush".len());
                unsafe { device.destroy_semaphore(semaphore, None) }
            }
        }
    }
}

pub(crate) unsafe extern "system" fn create_semaphore(
    device: vk::Device,
    p_create_info: *const vk::SemaphoreCreateInfo,
    p_allocator: *const vk::AllocationCallbacks,
    p_semaphore: *mut vk::Semaphore,
) -> vk::Result {
    let result = catch_unwind(|| {
        let create_info: MySemaphoreCreateInfo = p_create_info.into();

        let go = || {
            dbg!("creating".len());
            let lock = super::DEVICE.read().unwrap();
            let device = &lock.get(&device).unwrap().device;
            unsafe {
                device.create_semaphore(p_create_info.as_ref().unwrap(), p_allocator.as_ref())
            }
        };

        if unsafe { p_create_info.read() }.p_next.is_null() {
            use std::collections::hash_map::Entry::*;
            let mut h = SEMAPHORE.lock().unwrap();
            let enabled = h.2;
            match h.1.entry(create_info) {
                Occupied(mut x) if enabled => {
                    dbg!("from cache".len());
                    let xa = x.get_mut().pop();
                    xa.map_or_else(go, Result::Ok)
                }
                _ => go(),
            }
            .map(|x| {
                h.0.insert(x, create_info);
                unsafe { *p_semaphore = x };
                vk::Result::SUCCESS
            })
        } else {
            dbg!("skipping cache".len());
            go().map(|x| {
                unsafe { *p_semaphore = x };
                vk::Result::SUCCESS
            })
        }
    });
    match result.unwrap() {
        Ok(x) => x,
        Err(x) => x,
    }
}

pub(crate) unsafe extern "system" fn destroy_semaphore(
    device: vk::Device,
    semaphore: vk::Semaphore,
    p_allocator: *const vk::AllocationCallbacks,
) {
    let result = catch_unwind(|| {
        let mut h = SEMAPHORE.lock().unwrap();
        if h.2
            && let Some(y) = h.0.remove(&semaphore)
        {
            dbg!("save".len());
            use std::collections::hash_map::Entry::*;
            match h.1.entry(y) {
                Occupied(mut x) => {
                    x.get_mut().push(semaphore);
                }
                Vacant(x) => {
                    x.insert(vec![semaphore]);
                }
            }
        } else {
            dbg!("destroy".len());
            let lock = super::DEVICE.read().unwrap();
            let device = &lock.get(&device).unwrap().device;
            unsafe { device.destroy_semaphore(semaphore, p_allocator.as_ref()) }
        }
    });
    result.unwrap();
}
