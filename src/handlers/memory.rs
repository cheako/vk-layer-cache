// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::panic::catch_unwind;
use std::sync::LazyLock;
use std::sync::Mutex;

use ash::vk;

use super::types::*;

pub(super) struct Memorys(
    pub(super) HashMap<vk::DeviceMemory, MyMemoryAllocateInfo>,
    pub(super) HashMap<MyMemoryAllocateInfo, Vec<vk::DeviceMemory>>,
    pub(super) bool,
);

#[allow(clippy::type_complexity)]
pub(super) static MEMORY: LazyLock<Mutex<Memorys>> = LazyLock::new(|| {
    Memorys(
        Default::default(),
        Default::default(),
        dbg!(std::env::var("CHEAKO_M").is_ok()),
    )
    .into()
});

impl Memorys {
    pub(crate) fn end_frame(&mut self, device: &ash::Device) {
        for (_, objects) in self.1.drain() {
            for memory in objects.into_iter() {
                dbg!("flush".len());
                unsafe { device.free_memory(memory, None) }
            }
        }
    }
}

pub(crate) unsafe extern "system" fn allocate_memory(
    device: vk::Device,
    p_allocate_info: *const vk::MemoryAllocateInfo,
    p_allocator: *const vk::AllocationCallbacks,
    p_memory: *mut vk::DeviceMemory,
) -> vk::Result {
    let result = catch_unwind(|| {
        let allocate_info: MyMemoryAllocateInfo = p_allocate_info.into();

        let go = || {
            dbg!("creating".len());
            super::DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .device
                .allocate_memory(p_allocate_info.as_ref().unwrap(), p_allocator.as_ref())
        };

        if if let Some(i) = allocate_info.image {
            let global = super::images::image::IMAGE.lock().unwrap();
            global.0.contains_key(&i)
        } else {
            true
        } && if let Some(b) = allocate_info.buffer {
            let global = super::buffer::BUFFER.lock().unwrap();
            global.0.contains_key(&b)
        } else {
            true
        } {
            use std::collections::hash_map::Entry::*;
            let mut h = MEMORY.lock().unwrap();
            let enabled = h.2;
            match h.1.entry(allocate_info) {
                Occupied(mut x) if enabled => {
                    dbg!("from cache".len());
                    let xa = x.get_mut().pop();
                    xa.map_or_else(go, Result::Ok)
                }
                _ => go(),
            }
            .map(|x| {
                h.0.insert(x, allocate_info);
                *p_memory = x;
                vk::Result::SUCCESS
            })
        } else {
            dbg!("skipping cache".len());
            go().map(|x| {
                *p_memory = x;
                vk::Result::SUCCESS
            })
        }
    });
    match result.unwrap() {
        Ok(x) => x,
        Err(x) => x,
    }
}

pub(crate) unsafe extern "system" fn free_memory(
    device: vk::Device,
    memory: vk::DeviceMemory,
    p_allocator: *const vk::AllocationCallbacks,
) {
    let result = catch_unwind(|| {
        let mut h = MEMORY.lock().unwrap();
        if h.2
            && let Some(y) = h.0.remove(&memory)
        {
            dbg!("save".len());
            use std::collections::hash_map::Entry::*;
            match h.1.entry(y) {
                Occupied(mut x) => {
                    x.get_mut().push(memory);
                }
                Vacant(x) => {
                    x.insert(vec![memory]);
                }
            }
        } else {
            dbg!("flush".len());
            super::DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .device
                .free_memory(memory, p_allocator.as_ref())
        }
    });
    result.unwrap()
}
