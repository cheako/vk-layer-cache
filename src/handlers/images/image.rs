// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::panic::catch_unwind;
use std::sync::LazyLock;
use std::sync::Mutex;

use ash::vk;

use super::types::*;

pub(crate) struct Images(
    pub(crate) HashMap<vk::Image, MyImageCreateInfo>,
    pub(crate) HashMap<MyImageCreateInfo, Vec<vk::Image>>,
    pub(crate) bool,
);

#[allow(clippy::type_complexity)]
pub(crate) static IMAGE: LazyLock<Mutex<Images>> = LazyLock::new(|| {
    Images(
        Default::default(),
        Default::default(),
        dbg!(std::env::var("CHEAKO_I").is_ok()),
    )
    .into()
});

impl Images {
    pub(crate) fn end_frame(&mut self, device: &ash::Device) {
        for (_, objects) in self.1.drain() {
            for image in objects.into_iter() {
                super::view::IMAGE_VIEW.lock().unwrap().flush_image(image);
                dbg!("destroy/flush".len());
                unsafe { device.destroy_image(image, None) }
            }
        }
    }
}

pub(crate) unsafe extern "system" fn create_image(
    device: vk::Device,
    p_create_info: *const vk::ImageCreateInfo,
    p_allocator: *const vk::AllocationCallbacks,
    p_image: *mut vk::Image,
) -> vk::Result {
    let result = catch_unwind(|| {
        let create_info: MyImageCreateInfo = p_create_info.into();

        let go = || {
            dbg!("creating".len());
            unsafe {
                super::DEVICE
                    .read()
                    .unwrap()
                    .get(&device)
                    .unwrap()
                    .device
                    .create_image(p_create_info.as_ref().unwrap(), p_allocator.as_ref())
            }
        };

        if !create_info.more {
            use std::collections::hash_map::Entry::*;
            let mut h = IMAGE.lock().unwrap();
            let enabled = h.2;
            let inner_create_info = create_info.clone();
            match h.1.entry(create_info) {
                Occupied(mut x) if enabled => {
                    dbg!("from cache".len());
                    match x.get_mut().pop() {
                        Some(x) => Result::Ok(x),
                        None => go(),
                    }
                }
                _ => go(),
            }
            .map(|x| {
                h.0.insert(x, inner_create_info);
                unsafe { p_image.write(x) };
                vk::Result::SUCCESS
            })
        } else {
            dbg!("skipping cache".len());
            go().map(|x| {
                unsafe { p_image.write(x) };
                vk::Result::SUCCESS
            })
        }
    });
    match result.unwrap() {
        Ok(x) => x,
        Err(x) => x,
    }
}

pub(crate) unsafe extern "system" fn destroy_image(
    device: vk::Device,
    image: vk::Image,
    p_allocator: *const vk::AllocationCallbacks,
) {
    let result = catch_unwind(|| {
        let mut h = IMAGE.lock().unwrap();
        if h.2
            && let Some(y) = h.0.remove(&image)
        {
            dbg!("save".len());
            use std::collections::hash_map::Entry::*;
            match h.1.entry(y) {
                Occupied(mut x) => {
                    x.get_mut().push(image);
                }
                Vacant(x) => {
                    x.insert(vec![image]);
                }
            }
        } else {
            dbg!("destroy".len());
            super::view::IMAGE_VIEW.lock().unwrap().flush_image(image);
            unsafe {
                super::DEVICE
                    .read()
                    .unwrap()
                    .get(&device)
                    .unwrap()
                    .device
                    .destroy_image(image, p_allocator.as_ref())
            };
        }
    });
    result.unwrap()
}
