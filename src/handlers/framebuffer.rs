// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::panic::catch_unwind;
use std::sync::LazyLock;
use std::sync::Mutex;

use ash::vk;

use super::types::*;

pub(super) struct Framebuffers(
    pub(super) HashMap<vk::Framebuffer, MyFramebufferCreateInfo>,
    pub(super) HashMap<MyFramebufferCreateInfo, Vec<(usize, vk::Framebuffer)>>,
    pub(super) bool,
);

#[allow(clippy::type_complexity)]
pub(super) static FRAMEBUFFER: LazyLock<Mutex<Framebuffers>> = LazyLock::new(|| {
    Framebuffers(
        Default::default(),
        Default::default(),
        dbg!(std::env::var("CHEAKO_A").is_ok()),
    )
    .into()
});

impl Framebuffers {
    fn end_frame(&mut self, device: &ash::Device) {
        let mut next: HashMap<_, _> = Default::default();
        for (k, objects) in self.1.drain() {
            let mut v = vec![];
            for mut framebuffer in objects.into_iter() {
                framebuffer.0 -= 1;
                if framebuffer.0 == 0 {
                    dbg!("flush".len());
                    unsafe { device.destroy_framebuffer(framebuffer.1, None) }
                } else {
                    v.push(framebuffer);
                }
            }
            if !v.is_empty() {
                next.insert(k, v);
            }
        }
        self.1 = next;
    }
}

pub(crate) fn end_frame(device: &ash::Device) {
    let mut global = FRAMEBUFFER.lock().unwrap();
    global.end_frame(device);
}

pub(crate) unsafe extern "system" fn create_framebuffer(
    device: vk::Device,
    p_create_info: *const vk::FramebufferCreateInfo,
    p_allocator: *const vk::AllocationCallbacks,
    p_framebuffer: *mut vk::Framebuffer,
) -> vk::Result {
    let result = catch_unwind(|| {
        let create_info: MyFramebufferCreateInfo = p_create_info.into();

        let go = || {
            dbg!("creating".len());
            super::DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .device
                .create_framebuffer(p_create_info.as_ref().unwrap(), p_allocator.as_ref())
        };

        if p_create_info.read().p_next.is_null() {
            use std::collections::hash_map::Entry::*;
            let mut global = FRAMEBUFFER.lock().unwrap();
            let enabled = global.2;
            match global.1.entry(create_info.clone()) {
                Occupied(mut x) if enabled => {
                    dbg!("from cache".len());
                    let xa = x.get_mut().pop();
                    xa.map(|x| x.1).map_or_else(go, Result::Ok)
                }
                _ => go(),
            }
            .map(|x| {
                global.0.insert(x, create_info);
                *p_framebuffer = x;
                vk::Result::SUCCESS
            })
        } else {
            dbg!("skipping cache".len());
            go().map(|x| {
                *p_framebuffer = x;
                vk::Result::SUCCESS
            })
        }
    });
    match result.unwrap() {
        Err(x) => x,
        Ok(x) => x,
    }
}

pub(crate) unsafe extern "system" fn destroy_framebuffer(
    device: vk::Device,
    framebuffer: vk::Framebuffer,
    p_allocator: *const vk::AllocationCallbacks,
) {
    let result = catch_unwind(|| {
        let mut h = FRAMEBUFFER.lock().unwrap();
        if h.2
            && let Some(y) = h.0.remove(&framebuffer)
        {
            dbg!("save".len());
            use std::collections::hash_map::Entry::*;
            match h.1.entry(y) {
                Occupied(mut x) => {
                    x.get_mut().push((4, framebuffer));
                }
                Vacant(x) => {
                    x.insert(vec![(4, framebuffer)]);
                }
            }
        } else {
            dbg!("destroy".len());
            super::DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .device
                .destroy_framebuffer(framebuffer, p_allocator.as_ref())
        }
    });
    result.unwrap();
}
