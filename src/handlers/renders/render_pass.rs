// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::panic::catch_unwind;
use std::sync::LazyLock;
use std::sync::Mutex;

use ash::vk;

use super::types::*;

pub(crate) struct RenderPass(
    pub(crate) HashMap<MyRenderPassCreateInfo, (vk::RenderPass, ash::Device)>,
    pub(crate) bool,
);

#[allow(clippy::type_complexity)]
pub(crate) static RENDER_PASS: LazyLock<Mutex<RenderPass>> = LazyLock::new(|| {
    RenderPass(Default::default(), dbg!(std::env::var("CHEAKO_P").is_ok())).into()
});

impl RenderPass {
    pub(crate) fn end_frame(&mut self) {
        // Keeping all the RP.
    }
}

pub(crate) unsafe extern "system" fn create_render_pass(
    device: vk::Device,
    p_create_info: *const vk::RenderPassCreateInfo,
    p_allocator: *const vk::AllocationCallbacks,
    p_render_pass: *mut vk::RenderPass,
) -> vk::Result {
    let result = catch_unwind(|| {
        let create_info: MyRenderPassCreateInfo = p_create_info.into();

        let go = || {
            dbg!("creating".len());
            super::DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .device
                .create_render_pass(p_create_info.as_ref().unwrap(), p_allocator.as_ref())
        };

        if p_create_info.read().p_next.is_null() {
            use std::collections::hash_map::Entry::*;
            let mut h = RENDER_PASS.lock().unwrap();
            let enabled = h.1;
            let inner_create_info = create_info.clone();
            match h.0.entry(create_info) {
                Occupied(x) if enabled => {
                    dbg!("from cache".len());
                    Ok(x.get().0)
                }
                _ => go(),
            }
            .map(|val| {
                h.0.insert(
                    inner_create_info,
                    (
                        val,
                        super::DEVICE
                            .read()
                            .unwrap()
                            .get(&device)
                            .unwrap()
                            .device
                            .clone(),
                    ),
                );
                unsafe { p_render_pass.write(val) };
                vk::Result::SUCCESS
            })
        } else {
            dbg!("skipping cache".len());
            go().map(|val| {
                unsafe { p_render_pass.write(val) };
                vk::Result::SUCCESS
            })
        }
    });
    match result.unwrap() {
        Ok(x) => x,
        Err(x) => x,
    }
}

pub(crate) unsafe extern "system" fn destroy_render_pass(
    device: vk::Device,
    render_pass: vk::RenderPass,
    p_allocator: *const vk::AllocationCallbacks,
) {
    let result = catch_unwind(|| {
        let mut h = RENDER_PASS.lock().unwrap();
        if !h.1 {
            dbg!("destroy".len());
            for _ in h.0.drain().filter(|(_k, (v, _))| *v == render_pass) {}
            super::graphics_pipeline::GRAPHICS_PIPELINE
                .lock()
                .unwrap()
                .flush_render_pass(render_pass);
            super::DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .device
                .destroy_render_pass(render_pass, p_allocator.as_ref())
        }
    });
    result.unwrap();
}
