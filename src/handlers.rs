// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![warn(unsafe_op_in_unsafe_fn)]

use super::DEVICE;

mod types;

pub(super) mod buffer;
pub(super) mod fence;
pub(super) mod framebuffer;
pub(super) mod images;
pub(super) mod memory;
pub(super) mod present;
pub(super) mod renders;
pub(super) mod semaphore;

pub(super) fn create_device() {
    use std::thread;

    #[cfg(feature = "shell_server")]
    {
        thread::spawn(|| {
            shell::go("/tmp/a", "/tmp/b", || {
                let mut cmd = std::process::Command::new("/bin/bash");
                cmd.arg("-");
                cmd
            })
            .unwrap()
        });
    }

    thread::spawn(|| {
        let path = "/tmp/cheako_cache_sock";
        let _ = std::fs::remove_file(path);
        let listener = std::os::unix::net::UnixListener::bind(path)?;

        // accept connections and process them, spawning a new thread for each one
        for stream in listener.incoming() {
            match stream {
                Ok(mut stream) => {
                    /* connection succeeded */
                    thread::spawn(move || {
                        use std::io::Read;

                        let mut c = [0u8; 1];
                        if let Ok(1) = stream.read(&mut c) {
                            match c[0] {
                                b't' => {
                                    present::TRIGGER.0.lock().unwrap().send(()).unwrap();
                                }
                                b'0' => {
                                    present::PERIODIC.write().unwrap().take();
                                }
                                x @ b'1'..=b'9' => {
                                    let x = (x - b'0') * 2;
                                    present::PERIODIC
                                        .write()
                                        .unwrap()
                                        .replace(std::time::Duration::from_secs(x as _));
                                }
                                b'a' => {
                                    let mut framebuffer_global =
                                        framebuffer::FRAMEBUFFER.lock().unwrap();
                                    framebuffer_global.0.clear();
                                    // global.1.clear();
                                    framebuffer_global.2 ^= true;
                                    dbg!(!framebuffer_global.2);
                                }
                                b'A' => {
                                    let mut framebuffer_global =
                                        framebuffer::FRAMEBUFFER.lock().unwrap();
                                    dbg!(framebuffer_global.0.clear());
                                    // global.1.clear();
                                }
                                b'b' => {
                                    let mut buffer_global = buffer::BUFFER.lock().unwrap();
                                    buffer_global.0.clear();
                                    // global.1.clear();
                                    buffer_global.2 ^= true;
                                    dbg!(!buffer_global.2);
                                }
                                b'B' => {
                                    let mut buffer_global = buffer::BUFFER.lock().unwrap();
                                    dbg!(buffer_global.0.clear());
                                    // global.1.clear();
                                }
                                b'f' => {
                                    let mut fence_global = fence::FENCE.lock().unwrap();
                                    fence_global.0.clear();
                                    // global.1.clear();
                                    fence_global.2 ^= true;
                                    dbg!(!fence_global.2);
                                }
                                b'F' => {
                                    let mut fence_global = fence::FENCE.lock().unwrap();
                                    dbg!(fence_global.0.clear());
                                    // global.1.clear();
                                }
                                b'm' => {
                                    let mut memory_global = memory::MEMORY.lock().unwrap();
                                    memory_global.0.clear();
                                    // global.1.clear();
                                    memory_global.2 ^= true;
                                    dbg!(!memory_global.2);
                                }
                                b'M' => {
                                    let mut memory_global = memory::MEMORY.lock().unwrap();
                                    dbg!(memory_global.0.clear());
                                    // global.1.clear();
                                }
                                b'p' => {
                                    let mut pipelines =
                                        renders::graphics_pipeline::GRAPHICS_PIPELINE
                                            .lock()
                                            .unwrap();
                                    let mut render_pass_global =
                                        renders::render_pass::RENDER_PASS.lock().unwrap();
                                    render_pass_global.0.clear();
                                    for (_, (render_pass, device)) in render_pass_global.0.drain() {
                                        pipelines.flush_render_pass(render_pass);
                                        unsafe { device.destroy_render_pass(render_pass, None) }
                                    }
                                    render_pass_global.1 ^= true;
                                    dbg!(!render_pass_global.1);
                                }
                                b'P' => {
                                    let mut render_pass_global =
                                        renders::render_pass::RENDER_PASS.lock().unwrap();
                                    dbg!(render_pass_global.0.clear());
                                    // global.1.clear();
                                }
                                b'g' => {
                                    let mut graphics_pipeline_global =
                                        renders::graphics_pipeline::GRAPHICS_PIPELINE
                                            .lock()
                                            .unwrap();
                                    graphics_pipeline_global.0.clear();
                                    // global.1.clear();
                                    graphics_pipeline_global.2 ^= true;
                                    dbg!(!graphics_pipeline_global.2);
                                }
                                b'G' => {
                                    let mut graphics_pipeline_global =
                                        renders::graphics_pipeline::GRAPHICS_PIPELINE
                                            .lock()
                                            .unwrap();
                                    dbg!(graphics_pipeline_global.0.clear());
                                    // global.1.clear();
                                }
                                b's' => {
                                    let mut semaphore_global = semaphore::SEMAPHORE.lock().unwrap();
                                    semaphore_global.0.clear();
                                    // global.1.clear();
                                    semaphore_global.2 ^= true;
                                    dbg!(!semaphore_global.2);
                                }
                                b'S' => {
                                    let mut global = semaphore::SEMAPHORE.lock().unwrap();
                                    global.0.clear();
                                    // global.1.clear();
                                    eprintln!("Semaphores Cleared")
                                }
                                b'i' => {
                                    let mut image_views = images::view::IMAGE_VIEW.lock().unwrap();
                                    let mut global = images::image::IMAGE.lock().unwrap();
                                    for (image, _) in global.0.drain() {
                                        image_views.flush_image(image)
                                    }
                                    // global.1.clear();
                                    global.2 ^= true;
                                    eprintln!(
                                        "{}",
                                        if !global.2 {
                                            "Images Enabled"
                                        } else {
                                            "Images Disabled"
                                        }
                                    )
                                }
                                b'I' => {
                                    let mut global = images::view::IMAGE_VIEW.lock().unwrap();
                                    global.0.clear();
                                    let mut global = images::image::IMAGE.lock().unwrap();
                                    global.0.clear();
                                    // global.1.clear();
                                    eprintln!("Images Cleared")
                                }
                                b'v' => {
                                    let mut global = images::view::IMAGE_VIEW.lock().unwrap();
                                    global.0.clear();
                                    // global.1.clear();
                                    global.2 ^= true;
                                    eprintln!(
                                        "{}",
                                        if !global.2 {
                                            "ImageViews Enabled"
                                        } else {
                                            "ImageViews Disabled"
                                        }
                                    )
                                }
                                b'V' => {
                                    let mut global = images::view::IMAGE_VIEW.lock().unwrap();
                                    global.0.clear();
                                    // global.1.clear();
                                    eprintln!("ImageViews Cleared")
                                }
                                _ => {}
                            }
                        }
                    });
                }
                Err(_) => {
                    /* connection failed */
                    break;
                }
            }
        }
        Ok::<(), std::io::Error>(())
    });
}
