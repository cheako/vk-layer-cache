// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::panic::catch_unwind;
use std::slice;
use std::sync::LazyLock;
use std::sync::Mutex;

use ash::vk;

use super::super::types::FromVk;
use super::types::*;

pub(crate) struct GraphicsPipelines(
    pub(crate) HashMap<vk::Pipeline, MyGraphicsPipelineCreateInfo>,
    pub(crate) HashMap<MyGraphicsPipelineCreateInfo, (vk::Pipeline, usize, usize, ash::Device)>,
    pub(crate) bool,
);

#[allow(clippy::type_complexity)]
pub(crate) static GRAPHICS_PIPELINE: LazyLock<Mutex<GraphicsPipelines>> = LazyLock::new(|| {
    GraphicsPipelines(
        Default::default(),
        Default::default(),
        std::env::var("CHEAKO_P").is_ok() && dbg!(std::env::var("CHEAKO_G").is_ok()),
    )
    .into()
});

impl GraphicsPipelines {
    #[allow(unused_variables)]
    #[allow(unreachable_code)]
    pub(crate) fn end_frame(&mut self) {
        let mut next: HashMap<_, _> = Default::default();
        for (k, mut v) in self.1.drain() {
            v.2 -= 1;
            if v.2 == 0 {
                dbg!("flush".len());
                unsafe { v.3.destroy_pipeline(v.0, None) }
            } else {
                next.insert(k, v);
            }
        }
        self.1 = next;
    }

    pub(crate) fn flush_render_pass(&mut self, render_pass: vk::RenderPass) {
        self.1.retain(|x, (pipeline, ctr, _, device)| {
            if x.render_pass == render_pass {
                if *ctr <= 1 {
                    self.0.remove(pipeline);
                    dbg!("upstream leaving".len());
                    unsafe { device.destroy_pipeline(*pipeline, None) };
                    false
                } else {
                    *ctr -= 1;
                    true
                }
            } else {
                true
            }
        });
    }
}

pub(crate) unsafe extern "system" fn create_graphics_pipeline(
    device: vk::Device,
    pipeline_cache: vk::PipelineCache,
    count: u32,
    p_create_info: *const vk::GraphicsPipelineCreateInfo,
    p_allocator: *const vk::AllocationCallbacks,
    p_graphics_pipeline: *mut vk::Pipeline,
) -> vk::Result {
    let result = catch_unwind(|| {
        let create_infos: Box<[MyGraphicsPipelineCreateInfo]> = FromVk(count, p_create_info).into();

        let go = || {
            dbg!("creating".len());
            super::DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .device
                .create_graphics_pipelines(
                    pipeline_cache,
                    slice::from_raw_parts(p_create_info, count as _),
                    p_allocator.as_ref(),
                )
                .map_err(|(_, x)| x)
        };

        if count == 1 && create_infos[0].can_cache() {
            use std::collections::hash_map::Entry::*;
            let mut h = GRAPHICS_PIPELINE.lock().unwrap();
            let enabled = h.2;
            match h.1.entry(create_infos[0].clone()) {
                Occupied(mut x) if enabled => {
                    dbg!("from cache".len());
                    let (xa, n, _, _) = x.get_mut();
                    *n += 1;
                    Ok(vec![*xa])
                }
                Occupied(x) => {
                    x.remove();
                    go()
                }
                Vacant(x) if enabled => go().inspect(|y| {
                    x.insert((
                        y[0],
                        1,
                        4,
                        super::DEVICE
                            .read()
                            .unwrap()
                            .get(&device)
                            .unwrap()
                            .device
                            .clone(),
                    ));
                }),
                Vacant(_) => go(),
            }
            .map(|x| {
                h.0.insert(x[0], create_infos[0].clone());
                *p_graphics_pipeline = x[0];
                vk::Result::SUCCESS
            })
        } else {
            dbg!("skipping cache".len());
            go().map(|x| {
                for (i, pipeline) in x.into_iter().enumerate() {
                    *p_graphics_pipeline.add(i) = pipeline
                }
                vk::Result::SUCCESS
            })
        }
    });
    match result.unwrap() {
        Ok(x) => x,
        Err(x) => x,
    }
}

pub(crate) unsafe extern "system" fn destroy_pipeline(
    device: vk::Device,
    graphics_pipeline: vk::Pipeline,
    p_allocator: *const vk::AllocationCallbacks,
) {
    dbg!();
    let result = catch_unwind(|| {
        dbg!();
        let mut h = GRAPHICS_PIPELINE.lock().unwrap();
        dbg!();
        if h.2
            && let Some(y) = h.0.get(&graphics_pipeline).cloned()
        {
            dbg!("save".len());
            use std::collections::hash_map::Entry::*;
            match h.1.entry(y) {
                Occupied(mut x) => {
                    x.get_mut().1 -= 1;
                }
                Vacant(x) => {
                    x.insert((
                        graphics_pipeline,
                        0,
                        4,
                        super::DEVICE
                            .read()
                            .unwrap()
                            .get(&device)
                            .unwrap()
                            .device
                            .clone(),
                    ));
                }
            }
        } else {
            dbg!("destroy".len());
            super::DEVICE
                .read()
                .unwrap()
                .get(&device)
                .unwrap()
                .device
                .destroy_pipeline(graphics_pipeline, p_allocator.as_ref())
        }
    });
    result.unwrap();
}
