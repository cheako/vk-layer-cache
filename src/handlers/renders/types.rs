// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![allow(unused_imports)]

use ash::vk;
use either::Either;
use std::ffi::{self, c_void};
use std::slice::from_raw_parts;

use super::super::types::{FromVk, FromVk2, VkIter};

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyAttachmentDescription {
    pub flags: vk::AttachmentDescriptionFlags,
    pub format: vk::Format,
    pub samples: vk::SampleCountFlags,
    pub load_op: vk::AttachmentLoadOp,
    pub store_op: vk::AttachmentStoreOp,
    pub stencil_load_op: vk::AttachmentLoadOp,
    pub stencil_store_op: vk::AttachmentStoreOp,
    pub initial_layout: vk::ImageLayout,
    pub final_layout: vk::ImageLayout,
}

impl From<vk::AttachmentDescription> for MyAttachmentDescription {
    fn from(this: vk::AttachmentDescription) -> Self {
        Self {
            flags: this.flags,
            format: this.format,
            samples: this.samples,
            load_op: this.load_op,
            store_op: this.store_op,
            stencil_load_op: this.stencil_load_op,
            stencil_store_op: this.stencil_store_op,
            initial_layout: this.initial_layout,
            final_layout: this.final_layout,
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyAttachmentReference {
    pub attachment: u32,
    pub layout: vk::ImageLayout,
}

impl From<vk::AttachmentReference> for MyAttachmentReference {
    fn from(this: vk::AttachmentReference) -> Self {
        Self {
            attachment: this.attachment,
            layout: this.layout,
        }
    }
}

impl From<&vk::AttachmentReference> for MyAttachmentReference {
    fn from(this: &vk::AttachmentReference) -> Self {
        Self {
            attachment: this.attachment,
            layout: this.layout,
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MySubpassDescription {
    pub flags: vk::SubpassDescriptionFlags,
    pub pipeline_bind_point: vk::PipelineBindPoint,
    pub input_attachments: Box<[MyAttachmentReference]>,
    #[allow(clippy::type_complexity)]
    pub color_attachments:
        Either<Box<[(MyAttachmentReference, MyAttachmentReference)]>, Box<[MyAttachmentReference]>>,
    pub depth_stencil_attachment: Option<MyAttachmentReference>,
    pub preserve_attachments: Box<[u32]>,
}

impl<'a> From<vk::SubpassDescription<'a>> for MySubpassDescription {
    fn from(this: vk::SubpassDescription) -> Self {
        Self {
            flags: this.flags,
            pipeline_bind_point: this.pipeline_bind_point,
            input_attachments: FromVk(this.input_attachment_count, this.p_input_attachments).into(),
            color_attachments: if !this.p_resolve_attachments.is_null() {
                Either::Left(
                    FromVk2(
                        this.color_attachment_count,
                        this.p_color_attachments,
                        this.p_resolve_attachments,
                    )
                    .into(),
                )
            } else {
                Either::Right(FromVk(this.color_attachment_count, this.p_color_attachments).into())
            },
            depth_stencil_attachment: unsafe { this.p_depth_stencil_attachment.as_ref() }
                .map(Into::into),
            preserve_attachments: FromVk(
                this.preserve_attachment_count,
                this.p_preserve_attachments,
            )
            .into(),
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MySubpassDependency {
    pub src_subpass: u32,
    pub dst_subpass: u32,
    pub src_stage_mask: vk::PipelineStageFlags,
    pub dst_stage_mask: vk::PipelineStageFlags,
    pub src_access_mask: vk::AccessFlags,
    pub dst_access_mask: vk::AccessFlags,
}

impl From<vk::SubpassDependency> for MySubpassDependency {
    fn from(this: vk::SubpassDependency) -> Self {
        Self {
            src_subpass: this.src_subpass,
            dst_subpass: this.dst_subpass,
            src_stage_mask: this.src_stage_mask,
            dst_stage_mask: this.dst_stage_mask,
            src_access_mask: this.src_access_mask,
            dst_access_mask: this.dst_access_mask,
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyRenderPassCreateInfo {
    pub more: bool,
    pub flags: vk::RenderPassCreateFlags,
    pub attachments: Box<[MyAttachmentDescription]>,
    pub subpasses: Box<[MySubpassDescription]>,
    pub dependencies: Box<[MySubpassDependency]>,
}

impl<'a> From<*const vk::RenderPassCreateInfo<'a>> for MyRenderPassCreateInfo {
    fn from(this: *const vk::RenderPassCreateInfo) -> Self {
        let this = unsafe { this.read() };
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            attachments: FromVk(this.attachment_count, this.p_attachments).into(),
            subpasses: FromVk(this.subpass_count, this.p_subpasses).into(),
            dependencies: FromVk(this.dependency_count, this.p_dependencies).into(),
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MySpecializationMapEntry {
    pub constant_id: u32,
    pub offset: u32,
    pub size: usize,
}

impl From<vk::SpecializationMapEntry> for MySpecializationMapEntry {
    fn from(this: vk::SpecializationMapEntry) -> Self {
        Self {
            constant_id: this.constant_id,
            offset: this.offset,
            size: this.size,
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MySpecializationInfo {
    pub map_entries: Box<[MySpecializationMapEntry]>,
    pub data: Box<[u8]>,
}

impl<'a> From<&vk::SpecializationInfo<'a>> for MySpecializationInfo {
    fn from(this: &vk::SpecializationInfo) -> Self {
        Self {
            map_entries: FromVk(this.map_entry_count, this.p_map_entries).into(),
            data: FromVk(this.data_size as _, this.p_data as *const u8).into(),
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyPipelineShaderStageCreateInfo {
    pub more: bool,
    pub flags: vk::PipelineShaderStageCreateFlags,
    pub stage: vk::ShaderStageFlags,
    pub module: vk::ShaderModule,
    pub name: ffi::CString,
    pub specialization_info: Option<MySpecializationInfo>,
}

impl<'a> From<vk::PipelineShaderStageCreateInfo<'a>> for MyPipelineShaderStageCreateInfo {
    fn from(this: vk::PipelineShaderStageCreateInfo) -> Self {
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            stage: this.stage,
            module: this.module,
            name: unsafe { ffi::CStr::from_ptr(this.p_name) }.into(),
            specialization_info: unsafe { this.p_specialization_info.as_ref() }.map(Into::into),
        }
    }
}

impl MyPipelineShaderStageCreateInfo {
    fn can_cache(&self) -> bool {
        !self.more
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyVertexInputBindingDescription {
    pub binding: u32,
    pub stride: u32,
    pub input_rate: vk::VertexInputRate,
}

impl From<vk::VertexInputBindingDescription> for MyVertexInputBindingDescription {
    fn from(this: vk::VertexInputBindingDescription) -> Self {
        Self {
            binding: this.binding,
            stride: this.stride,
            input_rate: this.input_rate,
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyVertexInputAttributeDescription {
    pub location: u32,
    pub binding: u32,
    pub format: vk::Format,
    pub offset: u32,
}

impl From<vk::VertexInputAttributeDescription> for MyVertexInputAttributeDescription {
    fn from(this: vk::VertexInputAttributeDescription) -> Self {
        Self {
            location: this.location,
            binding: this.binding,
            format: this.format,
            offset: this.offset,
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyPipelineVertexInputStateCreateInfo {
    pub more: bool,
    pub flags: vk::PipelineVertexInputStateCreateFlags,
    pub p_vertex_binding_descriptions: Box<[MyVertexInputBindingDescription]>,
    pub p_vertex_attribute_descriptions: Box<[MyVertexInputAttributeDescription]>,
}

impl<'a> From<&vk::PipelineVertexInputStateCreateInfo<'a>>
    for MyPipelineVertexInputStateCreateInfo
{
    fn from(this: &vk::PipelineVertexInputStateCreateInfo) -> Self {
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            p_vertex_binding_descriptions: FromVk(
                this.vertex_binding_description_count,
                this.p_vertex_binding_descriptions,
            )
            .into(),
            p_vertex_attribute_descriptions: FromVk(
                this.vertex_attribute_description_count,
                this.p_vertex_attribute_descriptions,
            )
            .into(),
        }
    }
}

impl MyPipelineVertexInputStateCreateInfo {
    fn can_cache(&self) -> bool {
        !self.more
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyPipelineInputAssemblyStateCreateInfo {
    pub more: bool,
    pub flags: vk::PipelineInputAssemblyStateCreateFlags,
    pub topology: vk::PrimitiveTopology,
    pub primitive_restart_enable: bool,
}

impl<'a> From<&vk::PipelineInputAssemblyStateCreateInfo<'a>>
    for MyPipelineInputAssemblyStateCreateInfo
{
    fn from(this: &vk::PipelineInputAssemblyStateCreateInfo) -> Self {
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            topology: this.topology,
            primitive_restart_enable: this.primitive_restart_enable != 0,
        }
    }
}

impl MyPipelineInputAssemblyStateCreateInfo {
    fn can_cache(&self) -> bool {
        !self.more
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyPipelineTessellationStateCreateInfo {
    pub more: bool,
    pub flags: vk::PipelineTessellationStateCreateFlags,
    pub patch_control_points: u32,
}

impl<'a> From<&vk::PipelineTessellationStateCreateInfo<'a>>
    for MyPipelineTessellationStateCreateInfo
{
    fn from(this: &vk::PipelineTessellationStateCreateInfo) -> Self {
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            patch_control_points: this.patch_control_points,
        }
    }
}

impl MyPipelineTessellationStateCreateInfo {
    fn can_cache(&self) -> bool {
        !self.more
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyViewport {
    pub x: u32,
    pub y: u32,
    pub width: u32,
    pub height: u32,
    pub min_depth: u32,
    pub max_depth: u32,
}

impl From<vk::Viewport> for MyViewport {
    fn from(this: vk::Viewport) -> Self {
        Self {
            x: this.x.to_bits(),
            y: this.y.to_bits(),
            width: this.width.to_bits(),
            height: this.height.to_bits(),
            min_depth: this.min_depth.to_bits(),
            max_depth: this.max_depth.to_bits(),
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyOffset2D {
    pub x: i32,
    pub y: i32,
}

impl From<vk::Offset2D> for MyOffset2D {
    fn from(this: vk::Offset2D) -> Self {
        Self {
            x: this.x,
            y: this.y,
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyExtent2D {
    pub width: u32,
    pub height: u32,
}

impl From<vk::Extent2D> for MyExtent2D {
    fn from(this: vk::Extent2D) -> Self {
        Self {
            width: this.width,
            height: this.height,
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyRect2D {
    pub offset: MyOffset2D,
    pub extent: MyExtent2D,
}

impl From<vk::Rect2D> for MyRect2D {
    fn from(this: vk::Rect2D) -> Self {
        Self {
            offset: this.offset.into(),
            extent: this.extent.into(),
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyPipelineViewportStateCreateInfo {
    pub more: bool,
    pub flags: vk::PipelineViewportStateCreateFlags,
    pub viewports: Box<[MyViewport]>,
    pub scissors: Box<[MyRect2D]>,
}

impl<'a> From<&vk::PipelineViewportStateCreateInfo<'a>> for MyPipelineViewportStateCreateInfo {
    fn from(this: &vk::PipelineViewportStateCreateInfo) -> Self {
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            viewports: FromVk(this.viewport_count, this.p_viewports).into(),
            scissors: FromVk(this.scissor_count, this.p_scissors).into(),
        }
    }
}

impl MyPipelineViewportStateCreateInfo {
    fn can_cache(&self) -> bool {
        !self.more
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyPipelineRasterizationStateCreateInfo {
    pub more: bool,
    pub flags: vk::PipelineRasterizationStateCreateFlags,
    pub depth_clamp_enable: bool,
    pub rasterizer_discard_enable: bool,
    pub polygon_mode: vk::PolygonMode,
    pub cull_mode: vk::CullModeFlags,
    pub front_face: vk::FrontFace,
    pub depth_bias_enable: bool,
    pub depth_bias_constant_factor: u32,
    pub depth_bias_clamp: u32,
    pub depth_bias_slope_factor: u32,
    pub line_width: u32,
}

impl<'a> From<*const vk::PipelineRasterizationStateCreateInfo<'a>>
    for MyPipelineRasterizationStateCreateInfo
{
    fn from(this: *const vk::PipelineRasterizationStateCreateInfo) -> Self {
        let this = unsafe { this.read() };
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            depth_clamp_enable: this.depth_clamp_enable != 0,
            rasterizer_discard_enable: this.rasterizer_discard_enable != 0,
            polygon_mode: this.polygon_mode,
            cull_mode: this.cull_mode,
            front_face: this.front_face,
            depth_bias_enable: this.depth_bias_enable != 0,
            depth_bias_constant_factor: this.depth_bias_constant_factor.to_bits(),
            depth_bias_clamp: this.depth_bias_clamp.to_bits(),
            depth_bias_slope_factor: this.depth_bias_slope_factor.to_bits(),
            line_width: this.line_width.to_bits(),
        }
    }
}

impl MyPipelineRasterizationStateCreateInfo {
    fn can_cache(&self) -> bool {
        !self.more
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyPipelineMultisampleStateCreateInfo {
    pub more: bool,
    pub flags: vk::PipelineMultisampleStateCreateFlags,
    pub rasterization_samples: vk::SampleCountFlags,
    pub sample_shading_enable: bool,
    pub min_sample_shading: u32,
    pub sample_mask: Box<[vk::SampleMask]>,
    pub alpha_to_coverage_enable: bool,
    pub alpha_to_one_enable: bool,
}

impl<'a> From<&vk::PipelineMultisampleStateCreateInfo<'a>>
    for MyPipelineMultisampleStateCreateInfo
{
    fn from(this: &vk::PipelineMultisampleStateCreateInfo) -> Self {
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            rasterization_samples: this.rasterization_samples,
            sample_shading_enable: this.sample_shading_enable != 0,
            min_sample_shading: this.min_sample_shading.to_bits(),
            sample_mask: FromVk(
                (this.rasterization_samples.as_raw() + 31) / 32,
                this.p_sample_mask,
            )
            .into(),
            alpha_to_coverage_enable: this.alpha_to_coverage_enable != 0,
            alpha_to_one_enable: this.alpha_to_one_enable != 0,
        }
    }
}

impl MyPipelineMultisampleStateCreateInfo {
    fn can_cache(&self) -> bool {
        !self.more
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyStencilOpState {
    pub fail_op: vk::StencilOp,
    pub pass_op: vk::StencilOp,
    pub depth_fail_op: vk::StencilOp,
    pub compare_op: vk::CompareOp,
    pub compare_mask: u32,
    pub write_mask: u32,
    pub reference: u32,
}

impl From<vk::StencilOpState> for MyStencilOpState {
    fn from(this: vk::StencilOpState) -> Self {
        Self {
            fail_op: this.fail_op,
            pass_op: this.pass_op,
            depth_fail_op: this.depth_fail_op,
            compare_op: this.compare_op,
            compare_mask: this.compare_mask,
            write_mask: this.write_mask,
            reference: this.reference,
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyPipelineDepthStencilStateCreateInfo {
    pub more: bool,
    pub flags: vk::PipelineDepthStencilStateCreateFlags,
    pub depth_test_enable: bool,
    pub depth_write_enable: bool,
    pub depth_compare_op: vk::CompareOp,
    pub depth_bounds_test_enable: bool,
    pub stencil_test_enable: bool,
    pub front: MyStencilOpState,
    pub back: MyStencilOpState,
    pub min_depth_bounds: u32,
    pub max_depth_bounds: u32,
}

impl<'a> From<&vk::PipelineDepthStencilStateCreateInfo<'a>>
    for MyPipelineDepthStencilStateCreateInfo
{
    fn from(this: &vk::PipelineDepthStencilStateCreateInfo) -> Self {
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            depth_test_enable: this.depth_test_enable != 0,
            depth_write_enable: this.depth_write_enable != 0,
            depth_compare_op: this.depth_compare_op,
            depth_bounds_test_enable: this.depth_bounds_test_enable != 0,
            stencil_test_enable: this.stencil_test_enable != 0,
            front: this.front.into(),
            back: this.back.into(),
            min_depth_bounds: this.min_depth_bounds.to_bits(),
            max_depth_bounds: this.max_depth_bounds.to_bits(),
        }
    }
}

impl MyPipelineDepthStencilStateCreateInfo {
    fn can_cache(&self) -> bool {
        !self.more
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyPipelineColorBlendAttachmentState {
    pub blend_enable: bool,
    pub src_color_blend_factor: vk::BlendFactor,
    pub dst_color_blend_factor: vk::BlendFactor,
    pub color_blend_op: vk::BlendOp,
    pub src_alpha_blend_factor: vk::BlendFactor,
    pub dst_alpha_blend_factor: vk::BlendFactor,
    pub alpha_blend_op: vk::BlendOp,
    pub color_write_mask: vk::ColorComponentFlags,
}

impl From<vk::PipelineColorBlendAttachmentState> for MyPipelineColorBlendAttachmentState {
    fn from(this: vk::PipelineColorBlendAttachmentState) -> Self {
        Self {
            blend_enable: this.blend_enable != 0,
            src_color_blend_factor: this.src_color_blend_factor,
            dst_color_blend_factor: this.dst_color_blend_factor,
            color_blend_op: this.color_blend_op,
            src_alpha_blend_factor: this.src_alpha_blend_factor,
            dst_alpha_blend_factor: this.dst_alpha_blend_factor,
            alpha_blend_op: this.alpha_blend_op,
            color_write_mask: this.color_write_mask,
        }
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyPipelineColorBlendStateCreateInfo {
    pub more: bool,
    pub flags: vk::PipelineColorBlendStateCreateFlags,
    pub logic_op_enable: bool,
    pub logic_op: vk::LogicOp,
    pub attachments: Box<[MyPipelineColorBlendAttachmentState]>,
    pub blend_constants: [u32; 4],
}

impl<'a> From<&vk::PipelineColorBlendStateCreateInfo<'a>> for MyPipelineColorBlendStateCreateInfo {
    fn from(this: &vk::PipelineColorBlendStateCreateInfo) -> Self {
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            logic_op_enable: this.logic_op_enable != 0,
            logic_op: this.logic_op,
            attachments: FromVk(this.attachment_count, this.p_attachments).into(),
            blend_constants: this.blend_constants.map(f32::to_bits),
        }
    }
}

impl MyPipelineColorBlendStateCreateInfo {
    fn can_cache(&self) -> bool {
        !self.more
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyPipelineDynamicStateCreateInfo {
    pub more: bool,
    pub flags: vk::PipelineDynamicStateCreateFlags,
    pub p_dynamic_states: Box<[vk::DynamicState]>,
}

impl<'a> From<&vk::PipelineDynamicStateCreateInfo<'a>> for MyPipelineDynamicStateCreateInfo {
    fn from(this: &vk::PipelineDynamicStateCreateInfo) -> Self {
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            p_dynamic_states: FromVk(this.dynamic_state_count, this.p_dynamic_states).into(),
        }
    }
}

impl MyPipelineDynamicStateCreateInfo {
    fn can_cache(&self) -> bool {
        !self.more
    }
}

#[cfg_attr(feature = "debug", derive(Debug))]
#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyGraphicsPipelineCreateInfo {
    pub more: bool,
    pub flags: vk::PipelineCreateFlags,
    pub stages: Box<[MyPipelineShaderStageCreateInfo]>,
    pub vertex_input_state: Option<MyPipelineVertexInputStateCreateInfo>,
    pub input_assembly_state: Option<MyPipelineInputAssemblyStateCreateInfo>,
    pub tessellation_state: Option<MyPipelineTessellationStateCreateInfo>,
    pub viewport_state: Option<MyPipelineViewportStateCreateInfo>,
    pub rasterization_state: MyPipelineRasterizationStateCreateInfo,
    pub multisample_state: Option<MyPipelineMultisampleStateCreateInfo>,
    pub depth_stencil_state: Option<MyPipelineDepthStencilStateCreateInfo>,
    pub color_blend_state: Option<MyPipelineColorBlendStateCreateInfo>,
    pub dynamic_state: Option<MyPipelineDynamicStateCreateInfo>,
    pub layout: vk::PipelineLayout,
    pub render_pass: vk::RenderPass,
    pub subpass: u32,
    pub base_pipeline_handle: vk::Pipeline,
    pub base_pipeline_index: i32,
}

impl<'a> From<vk::GraphicsPipelineCreateInfo<'a>> for MyGraphicsPipelineCreateInfo {
    fn from(this: vk::GraphicsPipelineCreateInfo) -> Self {
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            stages: FromVk(this.stage_count, this.p_stages).into(),
            vertex_input_state: unsafe { this.p_vertex_input_state.as_ref() }.map(Into::into),
            input_assembly_state: unsafe { this.p_input_assembly_state.as_ref() }.map(Into::into),
            tessellation_state: unsafe { this.p_tessellation_state.as_ref() }.map(Into::into),
            viewport_state: unsafe { this.p_viewport_state.as_ref() }.map(Into::into),
            rasterization_state: this.p_rasterization_state.into(),
            multisample_state: unsafe { this.p_multisample_state.as_ref() }.map(Into::into),
            depth_stencil_state: unsafe { this.p_depth_stencil_state.as_ref() }.map(Into::into),
            color_blend_state: unsafe { this.p_color_blend_state.as_ref() }.map(Into::into),
            dynamic_state: unsafe { this.p_dynamic_state.as_ref() }.map(Into::into),
            layout: this.layout,
            render_pass: this.render_pass,
            subpass: this.subpass,
            base_pipeline_handle: this.base_pipeline_handle,
            base_pipeline_index: this.base_pipeline_index,
        }
    }
}
impl MyGraphicsPipelineCreateInfo {
    pub(crate) fn can_cache(&self) -> bool {
        (!self.more)
            && self
                .stages
                .iter()
                .all(MyPipelineShaderStageCreateInfo::can_cache)
            && self
                .vertex_input_state
                .iter()
                .all(MyPipelineVertexInputStateCreateInfo::can_cache)
            && self
                .input_assembly_state
                .iter()
                .all(MyPipelineInputAssemblyStateCreateInfo::can_cache)
            && self
                .tessellation_state
                .iter()
                .all(MyPipelineTessellationStateCreateInfo::can_cache)
            && self
                .viewport_state
                .iter()
                .all(MyPipelineViewportStateCreateInfo::can_cache)
            && self.rasterization_state.can_cache()
            && self
                .multisample_state
                .iter()
                .all(MyPipelineMultisampleStateCreateInfo::can_cache)
            && self
                .depth_stencil_state
                .iter()
                .all(MyPipelineDepthStencilStateCreateInfo::can_cache)
            && self
                .color_blend_state
                .iter()
                .all(MyPipelineColorBlendStateCreateInfo::can_cache)
            && self
                .dynamic_state
                .iter()
                .all(MyPipelineDynamicStateCreateInfo::can_cache)
    }
}
