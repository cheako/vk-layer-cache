This layer was created to aid in debugging an issue I was having with just
about everything mesa.  Specifically I needed a way to inject SUBOPTIMAL
return values for QueuePresent.  This layer has a socket where one character
commands are issued to control various aspects.  While working with
No Man's Sky it was clear recreating the swapchain other objects were being
recreated.  This layer keeps track of destroyed objects and re-uses them,
caching.

There is an embedded shell server, separate from the command socket, this was
for interrogating Valve's steam sandbox.  By popular request I could be
convinced to remove it, I just don't think ppl will care.

Current example shell commands using netcat package:
  * A one time recreate swapchain `echo t | nc -U /tmp/cheako_cache_sock`
  * Give me a moment to switch windows and proceed resetting the swapchain every 8 seconds `sleep 7; echo 2 | nc -U /tmp/cheako_cache_sock`

Commands:
 * 0 disable SO injection, default
 * 1-9 every 2 seconds multiply SO injection (`CHEAKO_SECONDS=[seconds]`)
 * t a one-time trigger SO injection
 * f toggle fence caching, enabled (`CHEAKO_F=1`)
 * F one time clear fence cache
 * aA Framebuffer (`CHEAKO_A=1`)
 * bB Buffer (`CHEAKO_B=1`)
 * mM Memory (`CHEAKO_M=1`)
 * pP Render Pass (`CHEAKO_P=1`)
 * gG Graphics Pipeline (`CHEAKO_G=1`)
 * sS Semaphores (`CHEAKO_S=1`)
 * iI Images (`CHEAKO_I=1`)
 * vV ImageViews (`CHEAKO_V=1`)

Nothing prevents bad configurations like disable Images /w enabled ImageViews.

`CHEAKO_TARGET_FPS=y` is the same as `CHEAKO_TARGET_FPS=22` and has no
equivalent socket server command.