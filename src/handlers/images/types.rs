// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use ash::vk;

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct MyImageCreateInfo {
    pub more: bool,
    pub flags: vk::ImageCreateFlags,
    pub image_type: vk::ImageType,
    pub format: vk::Format,
    pub extent: vk::Extent3D,
    pub mip_levels: u32,
    pub array_layers: u32,
    pub samples: vk::SampleCountFlags,
    pub tiling: vk::ImageTiling,
    pub usage: vk::ImageUsageFlags,
    pub sharing_mode: vk::SharingMode,
    pub p_queue_family_indices: Box<[u32]>,
    pub initial_layout: vk::ImageLayout,
}

impl<'a> From<*const vk::ImageCreateInfo<'a>> for MyImageCreateInfo {
    fn from(this: *const vk::ImageCreateInfo) -> Self {
        let this = unsafe { this.read() };
        let queue_family_indices = if let Some(queue_family_indices) =
            unsafe { this.p_queue_family_indices.as_ref() }
        {
            unsafe {
                std::slice::from_raw_parts(queue_family_indices, this.queue_family_index_count as _)
            }
        } else {
            Default::default()
        }
        .iter()
        .copied()
        .collect();
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            image_type: this.image_type,
            format: this.format,
            extent: this.extent,
            mip_levels: this.mip_levels,
            array_layers: this.array_layers,
            samples: this.samples,
            tiling: this.tiling,
            usage: this.usage,
            sharing_mode: this.sharing_mode,
            p_queue_family_indices: queue_family_indices,
            initial_layout: this.initial_layout,
        }
    }
}

#[derive(Copy, Clone, Hash, PartialEq, Eq)]
pub struct MyImageViewCreateInfo {
    pub more: bool,
    pub flags: vk::ImageViewCreateFlags,
    pub image: vk::Image,
    pub view_type: vk::ImageViewType,
    pub format: vk::Format,
    pub r: vk::ComponentSwizzle,
    pub g: vk::ComponentSwizzle,
    pub b: vk::ComponentSwizzle,
    pub a: vk::ComponentSwizzle,
    pub aspect_mask: vk::ImageAspectFlags,
    pub base_mip_level: u32,
    pub level_count: u32,
    pub base_array_layer: u32,
    pub layer_count: u32,
}

impl<'a> From<*const vk::ImageViewCreateInfo<'a>> for MyImageViewCreateInfo {
    fn from(this: *const vk::ImageViewCreateInfo) -> Self {
        let this = unsafe { this.read() };
        Self {
            more: !this.p_next.is_null(),
            flags: this.flags,
            image: this.image,
            view_type: this.view_type,
            format: this.format,
            r: this.components.r,
            g: this.components.g,
            b: this.components.b,
            a: this.components.a,
            aspect_mask: this.subresource_range.aspect_mask,
            base_mip_level: this.subresource_range.base_mip_level,
            level_count: this.subresource_range.level_count,
            base_array_layer: this.subresource_range.base_array_layer,
            layer_count: this.subresource_range.layer_count,
        }
    }
}
